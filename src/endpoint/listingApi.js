const axios = require('axios');

const BYJUS_API_URL = "https://nut-case.s3.amazonaws.com/jobs.json";

export const fetchJobsListingsPayload = function () {
    return axios.get(BYJUS_API_URL)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        })
        .finally(function () {
            console.log("All set to fire....");
        });
}
