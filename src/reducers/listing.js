import {handleActions} from 'redux-actions';

export default handleActions(
    {
        FETCHING_PAYLOAD: (state, action) =>
            Object.assign({}, state, {fetchingPayload: action.payload}),
        STORE_FETCHED_LISTING_PAYLOAD: (state, action) =>
            Object.assign({}, state, {listingData: action.payload}),
        FILTERED_LISTING_PAYLOAD: (state, action) =>
            Object.assign({}, state, {filteredData: action.payload}),
    },
    {
        listingData: [],
        fetchingPayload: false,
        filteredData: []
    }
);
