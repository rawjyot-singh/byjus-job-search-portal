import React from "react";
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger'
import ReactDOM from "react-dom";

import App from "./components/App";

import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleWare = createSagaMiddleware()
const store = createStore(rootReducer, applyMiddleware(sagaMiddleWare, createLogger({})));
sagaMiddleWare.run(rootSaga);

ReactDOM.render(<Provider store={store}> <App/> </Provider>, document.getElementById("root"));
