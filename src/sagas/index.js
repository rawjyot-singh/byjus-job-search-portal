import "regenerator-runtime/runtime";
import {call, takeLatest, put} from 'redux-saga/effects';
import {fetchJobsListingsPayload} from "../endpoint/listingApi";
import {fetchingPayload, storeFetchedListingPayload} from "../actions/listings";


function* fetchJobsListingsPayloadSaga() {
    yield put(fetchingPayload(true));
    const listingPageResourse = yield call(fetchJobsListingsPayload.bind(this));
    if (listingPageResourse && listingPageResourse.status >= 200 && listingPageResourse.status <= 299 && listingPageResourse.data && listingPageResourse.data.data && listingPageResourse.data.data.length > 0) {
        yield put(storeFetchedListingPayload(listingPageResourse.data.data));
    } else {
        alert("Error Fetching Api data");
    }
    yield put(fetchingPayload(false));
}


export default function* rootSaga() {
    yield takeLatest('FETCH_JOBS_LISTINGS_PAYLOAD', fetchJobsListingsPayloadSaga)
}
