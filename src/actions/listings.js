import createAction from "redux-actions/es/createAction";

export const fetchJobsListingsPayload = createAction("FETCH_JOBS_LISTINGS_PAYLOAD");
export const fetchingPayload = createAction("FETCHING_PAYLOAD");
export const storeFetchedListingPayload = createAction("STORE_FETCHED_LISTING_PAYLOAD");
export const filteredListingPayload = createAction("FILTERED_LISTING_PAYLOAD");
