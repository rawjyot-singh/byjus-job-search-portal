export const capitialize = (splitedFilter) => {
    return splitedFilter.charAt(0).toUpperCase() + splitedFilter.slice(1).toLowerCase()
}

export const flatTheFilters = (extractedFilterDetails) => {
    let flatFilterDetails = [];
    extractedFilterDetails.map(filter => {
        let splitedFilters = filter.split(/[,/]+/);
        splitedFilters.map(splitedFilter => {
            splitedFilter = splitedFilter.trim();
            if (splitedFilter === "") return;
            if (splitedFilter.toUpperCase() === "NULL") return;

            !flatFilterDetails.includes(splitedFilter) && flatFilterDetails.push(capitialize(splitedFilter));
        })
    })
    return flatFilterDetails;
}

export const extractFilterDetails = (listingData, key) => {
    let extractFilterDetailsFromPayload = [];
    listingData.map(listingData => {
        if (!listingData || listingData === '') return;
        !extractFilterDetailsFromPayload.includes(listingData[key]) && extractFilterDetailsFromPayload.push(listingData[key])
    })

    return flatTheFilters(extractFilterDetailsFromPayload);
}
