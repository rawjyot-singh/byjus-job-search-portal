import React, {Component} from "react";

import SearchComponent from './body/searchpanels/SearchComponent';
import {withStyles} from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar';

import "./App.css";

const AppBarStyled = withStyles({
    root: {
        height: 100
    },
    colorPrimary: {
        backgroundColor: "#eaebf0"
    }

})(AppBar);

class App extends Component {
    render() {
        return (
            <div className="App">
                <AppBarStyled>
                    <div className="header-container">
                        <img src="https://cdn1.byjus.com/cbse/2017/09/15151739/BYJUS-LOGO-Purple-3kb.png"/>
                    </div>
                </AppBarStyled>
                <SearchComponent key={Math.random()}/>
            </div>
        );
    }
}

export default App;
