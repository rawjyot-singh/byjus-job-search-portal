import React, {Component} from "react";

import {connect} from 'react-redux';
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";

import {fetchJobsListingsPayload, filteredListingPayload} from "../../../actions/listings";

import './SearchComponent.css';

import FilterContainer from "../filterpanels/FilterContainer";
import ListingContainer from '../listingpanels/ListingContainer';

class SearchComponent extends Component {

    constructor(props) {
        super(props);
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(fetchJobsListingsPayload());
    }

    handleFilterChange(updatedFilterData) {
        const {dispatch} = this.props;
        dispatch(filteredListingPayload(updatedFilterData));
    }

    render() {
        const {listingData, filterData, fetchingPayload} = this.props;
        return <div>
            <div className="count-container">
                <Typography> TotalCount: {listingData ? listingData.length : 0}</Typography>
                <Typography> Search Result Count: {filterData ? filterData.length : 0}</Typography>
            </div>
            <div className="filter-container">
                <FilterContainer
                    listingData={listingData}
                    handleFilterChange={this.handleFilterChange}
                    {...this.props}
                />
            </div>
            {fetchingPayload && <LinearProgress color="secondary"/>}
            <div className="list-container">
                <ListingContainer
                    key={Math.random()}
                    listingData={filterData && filterData.length > 1 ? filterData : listingData}
                />
            </div>
        </div>
    }
}

function mapStateToProps(state) {
    return {
        fetchingPayload: state.listing.fetchingPayload,
        listingData: state.listing.listingData,
        filterData: state.listing.filteredData
    };
}

export default connect(mapStateToProps)(SearchComponent);
