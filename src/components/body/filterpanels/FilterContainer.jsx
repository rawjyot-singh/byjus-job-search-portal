import React from "react";

import NativeSelect from '@material-ui/core/Select';
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Button from '@material-ui/core/Button';

import './FilterContainer.css';

import {filteredListingPayload} from "../../../actions/listings";
import {extractFilterDetails} from "../../../utils/config";
import TextField from "@material-ui/core/TextField";

const FilterContainer = ({listingData, dispatch}) => {

    const [experience, setExperience] = React.useState('');
    const [location, setLocation] = React.useState('');
    const [skills, setSkills] = React.useState('');

    const handleChangeExperience = value => {
        setExperience(value);
    };
    const handleChangeLocation = value => {
        setLocation(value);
    };
    const handleChangeSkills = value => {
        setSkills(value);
    };

    const handleBlurForSkills = event => {
        let filterData = [];
        if (event.which === 13 || event.keyCode === 13) {
            listingData.map(listing => {
                skills !== '' && listing["skills"].toLowerCase().includes(skills.toLowerCase()) && filterData.push(listing);
            })
            dispatch(filteredListingPayload(filterData));
        }
    }

    const checkFilterStatus = (key, listing) => {
        switch (key) {
            case "experience":
                return experience !== '' && listing[key].toLowerCase().includes(experience.toLowerCase());
            case "location":
                return location !== '' && listing[key].toLowerCase().includes(location.toLowerCase());
            case "skills":
                return skills !== '' && listing[key].toLowerCase().includes(skills.toLowerCase());
        }
    }

    const searchWithUpdatedFilters = () => {
        let filterData = [];
        listingData.map(listing => {
            if (checkFilterStatus("experience", listing) || checkFilterStatus("location", listing) || checkFilterStatus("skills", listing)) {
                filterData.push(listing)
            }
        })
        dispatch(filteredListingPayload(filterData));
    }

    const resetResultFilters = () => {
        handleChangeExperience('');
        handleChangeLocation('');
        handleChangeSkills('');
        dispatch(filteredListingPayload([]));
    }

    return listingData.length > 1
        ? <div>
            <div className="filter-container-wrapper">
                <InputLabel htmlFor="Experience">Experience: </InputLabel>
                <NativeSelect value={experience} onChange={event => handleChangeExperience(event.target.value)}>
                    {
                        extractFilterDetails(listingData, "experience").map((exp, index) =>
                            <MenuItem value={exp} key={index + "experience"}>{exp}</MenuItem>)
                    }
                </NativeSelect>
                <InputLabel htmlFor="Location">Location: </InputLabel>
                <NativeSelect value={location} onChange={event => handleChangeLocation(event.target.value)}>
                    {
                        extractFilterDetails(listingData, "location").map((location, index) =>
                            <MenuItem value={location} key={index + "location"}>{location}</MenuItem>)
                    }
                </NativeSelect>
                <Button variant="contained" color="primary" onClick={searchWithUpdatedFilters}>
                    Search Job
                </Button>
                <Button variant="contained" color="secondary" onClick={resetResultFilters}>
                    Reset Search Results
                </Button>
            </div>
            <div className="filter-container-search-box">
                <TextField
                    id="standard-name"
                    label="Skills:"
                    value={skills}
                    placeholder="Press enter to search"
                    onChange={event => handleChangeSkills(event.target.value)}
                    onKeyUp={event => handleBlurForSkills(event)}
                    margin="normal"
                />
            </div>
        </div>
        : null
}

export default FilterContainer;
