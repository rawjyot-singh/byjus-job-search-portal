import React from "react";

import Card from '@material-ui/core/Card';
import {makeStyles} from '@material-ui/core/styles';
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    card: {
        minWidth: 275,
        margin: 10
    }
});

const listingContainer = ({listingData}) => {
    const classes = useStyles();
    if (!listingData || listingData.length < 1) return null;
    return listingData.map((employeeData, index) => <div>
        <Card className={classes.card} key={index + "cardKey" + Math.random()}>
            <CardContent key={index + "CardContent" + Math.random()}>
                <Typography key={index + "Typography1"}>
                    Jobtitle: {employeeData.title}
                </Typography>
                <Typography variant="body2" component="p" color="textSecondary"
                            key={index + "Typography1"}>
                    Company: {employeeData.companyname}
                </Typography>
                <Typography variant="body2" component="p" color="textSecondary"
                            key={index + "CardContent" + Math.random()}>
                    Location: {employeeData.location}
                </Typography>
                <Typography variant="body2" component="p" color="textSecondary"
                            key={index + "CardContent" + Math.random()}>
                    Salary: {employeeData.salary}
                </Typography>
                <Typography variant="body2" component="p" color="textSecondary"
                            key={index + "CardContent" + Math.random()}>
                    Experience: {employeeData.experience}
                </Typography>
                <Typography variant="body2" component="p" color="textSecondary"
                            key={index + "CardContent" + Math.random()}>
                    ApplyLink: {employeeData.applylink}
                </Typography>
                <Typography variant="body2" component="p" color="textSecondary"
                            key={index + "CardContent" + Math.random()}>
                    Skills: {employeeData.skills}
                </Typography>
            </CardContent>
        </Card>
    </div>)
}

export default listingContainer;



