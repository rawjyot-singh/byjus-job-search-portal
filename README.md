```BYJU'S Front end challenge:```

```Application Demo URL : https://byjus-search-app.herokuapp.com/```
Developer: Rawjyot Singh

Current App version: 1.0.0

```Features Included: ```
1) Search By skills.
2) Search by Experience and Location.
3) Listing of Jobs according to filters
4) Total Count of the Jobs
5) Filter Search Count


```Tech Details```
1) Stack Used to create Frontend Logiv : React Js, Redux, Redux-Saga
2) Stack Used to deployment and web-server: Express Webpack
3) code Writin in : ES6 (Babel used to transpile the code)
4) Material UI used to design the components
5) Used stateless components mostyl(One way binding).
6) Store from redux connected to Hight order components
7) Hooks used to mange state inside components.


```Dependencies Version``` 
React version : Latest 16.9.0
React-Dom version : Latest 16.9.0
Redux version : Latest 4.0.4


```Local Setup: ```
1) npm install
2) npm run dev
3) Browser will open localhost:8080


```Heroku Setup:```
1) Webpack -p
2) Push code to Heroku.






 





